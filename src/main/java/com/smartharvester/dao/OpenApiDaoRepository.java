package com.smartharvester.dao;


import com.smartharvester.model.openapi.OpenApi;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OpenApiDaoRepository extends MongoRepository<OpenApi, Integer> {

    @Query(value = "{'info.title' : { $regex : ?0 } }")
    List<OpenApi> findByTitleDescription(String regexp);

    @Query(value = "{'info.x-catalog-id' : ?0  }")
    OpenApi findByUUID(String uudi);


}