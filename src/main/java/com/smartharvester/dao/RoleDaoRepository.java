package com.smartharvester.dao;

import com.smartharvester.model.user.SmartHarvesterRole;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RoleDaoRepository extends MongoRepository<SmartHarvesterRole, String> {

    SmartHarvesterRole findByRole(String role);

}
