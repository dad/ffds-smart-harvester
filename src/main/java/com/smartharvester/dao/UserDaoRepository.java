package com.smartharvester.dao;

import com.smartharvester.model.openapi.OpenApi;
import com.smartharvester.model.user.SmartHarvesterUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserDaoRepository extends MongoRepository<SmartHarvesterUser, Integer> {

    Optional<SmartHarvesterUser> findByFirstName(String firstName);

    @Query(value = "{'email' : ?0  }")
    Optional<SmartHarvesterUser> findByEmail(String email);

    Boolean existsByEmail(String email);

}
