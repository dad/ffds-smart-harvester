package com.smartharvester;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@SpringBootApplication
public class SmartHarvesterApplication extends WebSecurityConfigurerAdapter {

	public static void main(String[] args) {

		SpringApplication.run(SmartHarvesterApplication.class, args);
	}


}
