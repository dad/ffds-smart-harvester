package com.smartharvester.service;

import com.smartharvester.model.openapi.OpenApi;

import java.util.List;


public interface OpenApiService {

    List<OpenApi> getAllOpenApi();

    List<String> getAllUrl();

    OpenApi getOpenApiByUUDI(String uui);

    List<OpenApi> getOpenApiByTitle(String titleDescription);

    void createOpenApi(OpenApi openApi);

}
