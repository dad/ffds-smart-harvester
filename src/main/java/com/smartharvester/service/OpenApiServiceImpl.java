package com.smartharvester.service;

import com.smartharvester.dao.OpenApiDaoRepository;
import com.smartharvester.model.openapi.OpenApi;
import com.smartharvester.model.openapi.OpenApiPathItem;
import com.smartharvester.model.openapi.OpenApiPathItemParameter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

public class OpenApiServiceImpl implements OpenApiService {

    @Autowired
    OpenApiDaoRepository openApiDaoRepository;

    @Override
    public List<OpenApi> getAllOpenApi() {
        return openApiDaoRepository.findAll();
    }


    @Override
    public List<String> getAllUrl() {
        List<OpenApi> listOfOpenApis = this.getAllOpenApi();
        List<String> allUrl = new ArrayList<>();
        if (!listOfOpenApis.isEmpty()) {
            for (OpenApi api : listOfOpenApis) {
                String servers = api.getServers().get(0).getUrl();
                String path = (String) api.getPaths().keySet().toArray()[0];
                String listOfParams = "?";
                for (Map.Entry<String, Map<String, OpenApiPathItem>> apiPathItem: api.getPaths().entrySet()) {
                    OpenApiPathItem mapOpenApiPathItem = (OpenApiPathItem) apiPathItem.getValue().values().toArray()[0];
                    List<OpenApiPathItemParameter> paramsList= mapOpenApiPathItem.getParameters();
                    for (OpenApiPathItemParameter parameter : paramsList) {
                        listOfParams = listOfParams.concat(parameter.getName() + "=" + parameter.getSchema().getDefaultValue() + "&");
                    }
                }
                listOfParams = StringUtils.substring(listOfParams, 0, listOfParams.length() - 1);
                allUrl.add(servers + path + listOfParams);
            }

        }
        return allUrl;
    }

    @Override
    public OpenApi getOpenApiByUUDI(String uui) {
        return openApiDaoRepository.findByUUID(uui);
    }

    @Override
    public List<OpenApi> getOpenApiByTitle(String titleDescription) {
        return openApiDaoRepository.findByTitleDescription(titleDescription);
    }

    @Override
    public void createOpenApi(OpenApi openApi) {
        openApiDaoRepository.save(openApi);
    }


}
