package com.smartharvester.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.smartharvester.model.user.SmartHarvesterUser;
import com.smartharvester.dao.UserDaoRepository;

@Service
public class UserServiceImpl implements UserDetailsService {

    @Autowired
    UserDaoRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName)  {
        SmartHarvesterUser user = userRepository
                .findByEmail(userName)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + userName));

        return UserService.build(user);
    }
}