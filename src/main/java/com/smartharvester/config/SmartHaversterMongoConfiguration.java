package com.smartharvester.config;

import com.smartharvester.security.services.UserServiceImpl;
import com.smartharvester.service.OpenApiService;
import com.smartharvester.service.OpenApiServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
@ComponentScan("com.smartharvester")
public class SmartHaversterMongoConfiguration {

    @Bean(name="openApiService")
    public OpenApiService getOpenApiService() {
        return new OpenApiServiceImpl();
    }

    @Bean(name="userService")
    public UserDetailsService getUserApiService() {
        return new UserServiceImpl();
    }
}
