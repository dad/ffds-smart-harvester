package com.smartharvester.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class SmartHarvesterHomeController {

    @GetMapping
    public String result() {
        return "Smart API for harvesting repositories";
    }
}
