package com.smartharvester.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.smartharvester.dao.OpenApiDaoRepository;
import com.smartharvester.exception.ResourceNotFoundException;
import com.smartharvester.model.openapi.OpenApi;
import com.smartharvester.service.OpenApiService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.parser.OpenAPIV3Parser;
import io.swagger.v3.parser.core.models.SwaggerParseResult;
import org.apache.http.HttpHost;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.apache.http.util.EntityUtils;
import org.bson.Document;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@CrossOrigin(origins = "*")
@RequestMapping("/harvester/api")
@Tag(name = "Harvest",description = "Harvest Repositories")
@RestController
public class SmartHarvesterController {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private OpenApiDaoRepository repository;

    @Autowired
    @Qualifier(value = "openApiService")
    OpenApiService openApiService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Method to fetch all openAPIs from mongodb.
     * @return
     */
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public Collection<OpenApi> getAll() {
        logger.debug("Getting all OpenApi from database...");
        List<OpenApi> response = openApiService.getAllOpenApi();
        return response;
    }

    /**
     * Method to fetch all openAPIs from mongodb.
     * @return
     */
    @RequestMapping(value = "/allurls", method = RequestMethod.GET)
    public Collection<String> getAllUrl() {
        logger.debug("Getting all OpenApi from database...");
        Collection<String> response = openApiService.getAllUrl();

        return response;
    }

    /**
     * Method to fetch an openAPI by its title description.
     * Uses a OpenApiRepository with queries
     * @see OpenApiDaoRepository
     * @return
     */
    @RequestMapping(value = "/title/{desc}", method = RequestMethod.GET)
    ResponseEntity<List<OpenApi>> filterByTitle(@PathVariable("desc") String titleDescription)
            throws ResourceNotFoundException {
        List<OpenApi> openApis = openApiService.getOpenApiByTitle(titleDescription);
        if (openApis.isEmpty()) {
            throw new ResourceNotFoundException("OpenAPI not found with title description '" + titleDescription + '\'');
        }
        return ResponseEntity.ok().body(openApis);
    }

    /**
     * Method to fetch an openAPI by its version.
     * Uses a MongoTemplate query
     * @return
     */

    @GetMapping("/getversion/{number}")
    ResponseEntity<List<OpenApi>> getAllUsers2(@PathVariable("number") String versionNumber)
            throws ResourceNotFoundException {
        List<OpenApi> openApis = mongoTemplate.query(OpenApi.class)
                .matching(query(where("info.version").regex(versionNumber)))
                .all();
        if (openApis.isEmpty()) {
            throw new ResourceNotFoundException("OpenAPI not found with version '" + versionNumber + '\'');
        }
        return ResponseEntity.ok().body(openApis);
    }



    /**
     * Method to fetch openApi by id.
     * @param id
     * @return
     */
    @GetMapping("/{uuid}")
    OpenApi getApiByUUDI(@PathVariable(value= "uuid") String id) {
        return openApiService.getOpenApiByUUDI(id);
    }

    @GetMapping (value="/openapi/{openapiId}/datasetlist")
    public String datasets(@PathVariable("openapiId") String openapiId) {
        OpenAPIV3Parser openapi = null;
        SwaggerParseResult api = null;
        Operation operation = null;
        String result = "";
        String server = "";
        String endpoint = "";
        String responseBody = "";
        JSONObject source = null;
        String urlapi = "";

        try{
            // function : search openapi description dans index elasticsearch
            String body = "{\"query\": {\"match\": {\"_id\": \""+ openapiId + "\"}}}";
            RestClient restClient = RestClient.builder(new HttpHost("172.17.0.2", 30942, "http")).build();
            Request request = new Request("GET","/index1/_search");
            request.setEntity(new NStringEntity(body,ContentType.APPLICATION_JSON));
            Response response = restClient.performRequest(request);
            int statusCode = response.getStatusLine().getStatusCode();
            responseBody = EntityUtils.toString(response.getEntity());
            JSONObject responsejson = new JSONObject(responseBody);
            JSONObject main = responsejson.getJSONObject("hits");
            String totalhits = main.getJSONObject("total").get("value").toString();

            System.out.println(totalhits);

            if (totalhits != "0") {

                JSONArray hits = main.getJSONArray("hits");
                JSONObject obj = hits.getJSONObject(0);
                source = obj.getJSONObject("_source");
                urlapi = source.getJSONArray("servers").getJSONObject(0).get("url").toString();

                server = "https://petstore3.swagger.io" + urlapi;
                // function Construire requete http avec la description openapi
                openapi = new OpenAPIV3Parser();
                api = openapi.readContents(source.toString());
                operation = api.getOpenAPI().getPaths().get("/pet/findByTags").getGet();

                //  System.out.println(operation.getOperationId());

                // lancer la requete et recupérer la liste des métadonnées des datasets
                endpoint = server + "/pet/findByTags?tags=tag1";
                URL url = new URL(endpoint);
                System.out.println(endpoint);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    result = readStream(in);
                } finally {
                    restClient.close();
                    urlConnection.disconnect();
                }
            } else {
                result = "Il n'y a pas de description openapi pour " + openapiId;
            }
        }
        catch (Exception e){
            System.out.println(e.toString());
        }
        return result.toString();
    }

    @GetMapping (value="/openapi/{openapiId}/individualdataset")
    public String dataset(@PathVariable("openapiId") String openapiId) {
        try{
        }
        catch (Exception e){
            System.out.println(e.toString());
        }
        return "Individual dataset content + metadata with api " + openapiId;
    }


    private String readStream(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
        for (String line = r.readLine(); line != null; line =r.readLine()){
            sb.append(line);
        }
        is.close();
        return sb.toString();
    }

}
