package com.smartharvester.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.smartharvester.dao.UserDaoRepository;
import com.smartharvester.exception.ResourceNotFoundException;
import com.smartharvester.model.openapi.OpenApi;
import com.smartharvester.model.user.SmartHarvesterUser;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@CrossOrigin(origins = "*")
@RestController
@Tag(name="SmartHarvester users", description = "User management")
@RequestMapping("/harvester/api")

public class SmartHarvesterUserController {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private UserDaoRepository userDaoRepository;

    /**
     * Method to fetch all users.
     * @return
     */
    @GetMapping("/allusers")
    public String getAllUsers() {
        MongoDatabase db = mongoTemplate.getDb();
        MongoIterable<String> names = db.listCollectionNames();
        names.forEach(n-> System.out.println("Name : " + n));
        Map<String, String> categotyLookUpMap = new HashMap<>();
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, String> users = new HashMap();
        names.forEach(n-> {
            if (n.equals("user")) {
                MongoCollection<Document> collection = db.getCollection(n);
                FindIterable<Document> iterDoc = collection.find();
                Iterator it = iterDoc.iterator();
                while (it.hasNext()) {
                    Document theObj = (Document) it.next();
                    System.out.println(theObj.getString("firstName"));
                    categotyLookUpMap.put(theObj.getString("firstName"), theObj.getString("lastName"));
                }
            }
        });

        if (mongoTemplate.collectionExists("user")){
            System.out.println(categotyLookUpMap);
            try {
                String json = objectMapper.writeValueAsString(categotyLookUpMap);
                System.out.println(json);
                return json;
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        };
        return "No";
    }

    /**
     * Method to fetch all users.
     * @return
     */

    @GetMapping("/allusers2")
    public Collection<SmartHarvesterUser> getAll2() {
        //logger.debug("Getting all OpenApi from database...");
        List<SmartHarvesterUser> response = userDaoRepository.findAll();
        return response;
    }

}
