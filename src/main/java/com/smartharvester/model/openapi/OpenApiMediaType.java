package com.smartharvester.model.openapi;

public class OpenApiMediaType {

    String type;

    OpenApiMediaTypeItems items;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public OpenApiMediaTypeItems getItems() {
        return items;
    }

    public void setItems(OpenApiMediaTypeItems items) {
        this.items = items;
    }
}
