package com.smartharvester.model.openapi;

import com.fasterxml.jackson.annotation.JsonInclude;

public class OpenApiPathItemParameter {

    String in;
    String name;
    OpenApiSchema schema;
    Boolean required;
    String description;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String example;


    public String getIn() {
        return in;
    }

    public void setIn(String in) {
        this.in = in;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OpenApiSchema getSchema() {
        return schema;
    }

    public void setSchema(OpenApiSchema schema) {
        this.schema = schema;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = example;
    }
}
