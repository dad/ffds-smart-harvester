package com.smartharvester.model.openapi;

import org.springframework.data.mongodb.core.mapping.Field;

public class OpenApiSchema {

    String type;
    @Field("default")
    String defaultValue;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }
}
