package com.smartharvester.model.openapi;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.UUID;

public class OpenApiInfo {

    String title;
    String version;
    @Field("x-format")
    @JsonProperty("x-format")
    String xFormat;

    @Field("x-result")
    @JsonProperty("x-result")
    String xResult;

    @Field("x-start-param")
    @JsonProperty("x-start-param")
    String xStartParam;

    @Field("x-page-param")
    @JsonProperty("x-page-param")
    String xPageParam;

    @Field("x-catalog-id")
    @JsonProperty("x-catalog-id")
    UUID xCatalogId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getxFormat() {
        return xFormat;
    }

    public void setxFormat(String xFormat) {
        this.xFormat = xFormat;
    }

    public String getxResult() {
        return xResult;
    }

    public void setxResult(String xResult) {
        this.xResult = xResult;
    }

    public String getxStartParam() {
        return xStartParam;
    }

    public void setxStartParam(String xStartParam) {
        this.xStartParam = xStartParam;
    }

    public String getxPageParam() {
        return xPageParam;
    }

    public void setxPageParam(String xPageParam) {
        this.xPageParam = xPageParam;
    }

    public UUID getxCatalogId() {
        return xCatalogId;
    }

    public void setxCatalogId(UUID xCatalogId) {
        this.xCatalogId = xCatalogId;
    }
}
