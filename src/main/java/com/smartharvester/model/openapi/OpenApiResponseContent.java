package com.smartharvester.model.openapi;

public class OpenApiResponseContent {

    OpenApiMediaType schema;

    public OpenApiMediaType getSchema() {
        return schema;
    }

    public void setSchema(OpenApiMediaType schema) {
        this.schema = schema;
    }
}

