package com.smartharvester.model.openapi;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Map;

@Document(collection = "openapi")
public class OpenApi {

    @Id
    @JsonIgnore
    String id;
    String openapi;
    OpenApiInfo info;
    List<OpenApiServer> servers;
    Map<String,Map<String,OpenApiPathItem>> paths;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOpenapi() {
        return openapi;
    }

    public void setOpenapi(String openapi) {
        this.openapi = openapi;
    }

    public OpenApiInfo getInfo() {
        return info;
    }

    public void setInfo(OpenApiInfo info) {
        this.info = info;
    }

    public List<OpenApiServer> getServers() {
        return servers;
    }

    public void setServers(List<OpenApiServer> servers) {
        this.servers = servers;
    }

    public Map<String, Map<String, OpenApiPathItem>> getPaths() {
        return paths;
    }

    public void setPaths(Map<String, Map<String, OpenApiPathItem>> paths) {
        this.paths = paths;
    }

    @Override
    public String toString() {
        return "OpenApi (id=" + id + ", infos = " + info + ")";
    }
}
