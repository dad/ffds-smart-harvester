package com.smartharvester.model.openapi;

public class OpenApiServer {

    String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
