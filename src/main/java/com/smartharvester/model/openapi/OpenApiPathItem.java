package com.smartharvester.model.openapi;

import java.util.List;
import java.util.Map;

public class OpenApiPathItem {

    String summary;
    List<String> tags;
    List <OpenApiPathItemParameter> parameters;
    Map<String,OpenApiResponse> responses;

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public List<OpenApiPathItemParameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<OpenApiPathItemParameter> parameters) {
        this.parameters = parameters;
    }

    public Map<String, OpenApiResponse> getResponses() {

        return responses;
    }

    public void setResponses(Map<String, OpenApiResponse> responses) {
        this.responses = responses;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }
}
