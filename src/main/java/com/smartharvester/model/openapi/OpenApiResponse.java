package com.smartharvester.model.openapi;

import java.util.Map;

public class OpenApiResponse {

    String description;
    Map<String,OpenApiResponseContent> content;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, OpenApiResponseContent> getContent() {
        return content;
    }

    public void setContent(Map<String, OpenApiResponseContent> content) {
        this.content = content;
    }
}
