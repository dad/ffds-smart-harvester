package com.smartharvester.model.openapi;

public class OpenApiMediaTypeItems {

    String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
